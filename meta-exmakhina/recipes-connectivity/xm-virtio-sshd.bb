# SPDX-License-Identifier: MIT
# Copyright (c) 2020-2021 Jérôme Carretero & contributors

SUMMARY = "SSH configuration data"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} += "socat"

SRC_URI=" \
 file://xm_stdio_multiplex.py \
 file://xm-virtio-sshd.service \
"

inherit systemd
SYSTEMD_SERVICE_${PN} = "xm-virtio-sshd.service"

S = "${WORKDIR}"

do_install() {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/xm-virtio-sshd.service ${D}${systemd_unitdir}/system

	install -d ${D}/usr/bin
	install -m 0755 ${WORKDIR}/xm_stdio_multiplex.py ${D}/usr/bin
}

FILES_${PN} = " \
 ${sysconfdir} \
 ${bindir} \
"
