#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Thing that allows to multiplex services (eg. SSH) over stdio
# SPDX-License-Identifier: GPL-3.0
# Copyright (c) 2020-2021 Jérôme Carretero & contributors

import sys, io, os
import subprocess
import fcntl
import socket
import logging
import select
import time
import marshal


def server(args):
	"""
	Take data from stdin, and either unpack and push to existing subprocess,
	or spawn new subprocess.
	Take data from subprocess, wrap with framing info and push to stdout.
	"""

	logger = logging.getLogger("server")

	stdin = sys.stdin.buffer.fileno()

	stream2proc = dict()
	stdout2stream = dict()


	while True:
		timeout = 1000.0
		logger.debug("Select?")
		rs = [stdin]
		for stream, proc in stream2proc.items():
			rs.append(proc.stdout)
		ws = []
		xs = []
		rs, ws, xs = select.select(rs, ws, xs, timeout)

		logger.debug("Select!")

		for r in rs:
			if r is stdin:

				logger.debug("Beg stdin")

				try:
					stream, datalen = marshal.load(sys.stdin.buffer)
					data = sys.stdin.buffer.read(datalen)
				except Exception as e:
					raise

				new_stream = False

				if not stream in stream2proc:
					new_stream = True

					proc = subprocess.Popen(
					 args.cmd,
					 stdout=subprocess.PIPE,
					 stdin=subprocess.PIPE,
					)

					logger.debug("New proc %s for stream %s", proc.stdout, stream)

					for x in (proc.stdout,):
						fd = x.fileno()
						fl = fcntl.fcntl(fd, fcntl.F_GETFL)
						fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

					stream2proc[stream] = proc
					stdout2stream[proc.stdout] = stream

				proc = stream2proc[stream]
				proc.stdin.write(data)
				proc.stdin.flush()

				if not new_stream and not data:
					del stdout2stream[proc.stdout]
					proc = stream2proc[stream]
					proc.terminate()
					del stream2proc[stream]

				logger.debug("End stdin (%d)", len(data))

			else:
				logger.debug("Beg stream %s", r)
				if not r in stdout2stream:
					logger.info("Zmb stream %s", r)
					continue

				stream = stdout2stream[r]
				proc = stream2proc[stream]

				incoming = os.read(r.fileno(), args.buffer_size)
				if not incoming:
					logger.info("Ded stream %s", r)
					stream = stdout2stream[r]
					del stdout2stream[r]
					proc = stream2proc[stream]
					proc.terminate()
					del stream2proc[stream]

				marshal.dump((stream, len(incoming)), sys.stdout.buffer)
				sys.stdout.buffer.write(incoming)
				sys.stdout.buffer.flush()

				logger.debug("End stream %s (%d)", r, len(incoming))


def client(args):
	"""
	Listen on a socket, accept new connections.
	For data on connections, wrap with framing info and push to stdout.
	Take data from stdin, unpack and push to existing client.
	"""

	logger = logging.getLogger("client")


	stdin = sys.stdin.buffer.fileno()

	con2client = dict()
	stream2con = dict()
	con2stream = dict()

	s_server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
	if os.path.exists(args.socket):
		os.unlink(args.socket)
	s_server.bind(args.socket)
	s_server.listen(1)

	try:
		while True:
			timeout = 1000.0
			logger.debug("Select?")
			rs = [stdin, s_server]
			for con in con2client.keys():
				rs.append(con)
			ws = []
			xs = []
			rs, ws, xs = select.select(rs, ws, xs, timeout)
			logger.debug("Select!")

			for r in rs:
				if r is stdin:
					logger.debug("Beg stdin")
					try:
						stream, datalen = marshal.load(sys.stdin.buffer)
						data = sys.stdin.buffer.read(datalen)
					except Exception as e:
						raise

					if not stream in stream2con:
						logger.debug("Res con %s", con)
						continue

					con = stream2con[stream]

					ded_con = False

					if not data:
						ded_con = True
						logger.debug("Ded con %s", con)

					try:
						con.sendall(data)
					except BrokenPipeError as e:
						logger.info("DeD con %s", con)
						ded_con = True

					if ded_con:
						con.close()
						del stream2con[stream]
						del con2client[con]
						del con2stream[con]

					logger.debug("End stdin (%d)", len(data))

				elif r is s_server:
					con, client = r.accept()
					logger.info("New client: %s", client)
					con.setblocking(0)
					con2client[con] = client
					stream = time.time()
					stream2con[stream] = con
					con2stream[con] = stream

					marshal.dump((stream, 0), sys.stdout.buffer)
					sys.stdout.buffer.flush()
				else:
					logger.debug("Beg forward %s", r)
					try:
						incoming = r.recv(args.buffer_size)
					except OSError:
						logger.info("Ded forward %s", r)
						assert r not in con2client

					client = con2client[r]
					stream = con2stream[r]

					if not incoming:
						logger.info("Ded forward %s", r)
						del con2client[r]
						del con2stream[r]
						del stream2con[stream]

					marshal.dump((stream, len(incoming)), sys.stdout.buffer)
					sys.stdout.buffer.write(incoming)
					sys.stdout.buffer.flush()
					logger.debug("End forward %s (%d)", r, len(incoming))
	finally:
		os.unlink(args.socket)


def main():
	import argparse

	parser = argparse.ArgumentParser(
	 description="stdio multiplex",
	)

	parser.add_argument("--log-level",
	 default="WARNING",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)


	subp = subparsers.add_parser(
	 "serverd",
	 help="Run server-side server",
	)

	subp.set_defaults(func=server)

	subp.add_argument("--buffer-size",
	 help="Amount of data to read from subprocess",
	 type=int,
	 default=65536,
	)

	subp.add_argument("cmd",
	 help="Command to run in each subprocess",
	 nargs=argparse.REMAINDER,
	)


	subp = subparsers.add_parser(
	 "clientd",
	 help="Run client-side server",
	)


	subp.set_defaults(func=client)

	subp.add_argument("--socket",
	 help="Socket to listen on",
	 default="tty.socket",
	)

	subp.add_argument("--buffer-size",
	 help="Amount of data to read from socket",
	 type=int,
	 default=65536,
	)


	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.root.setLevel(getattr(logging, args.log_level))

	if 1:
		logging.basicConfig(
		 level=getattr(logging, args.log_level),
		 format="%(name)s %(levelname)s %(message)s"
		)

	if getattr(args, 'func', None) is None:
		parser.print_help()
		raise SystemExit(1)
	else:
		args.func(args)
		raise SystemExit()


if __name__ == '__main__':
	ret = main()
	raise SystemExit(ret)
