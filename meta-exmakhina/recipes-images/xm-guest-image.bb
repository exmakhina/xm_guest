IMAGE_FSTYPES = "squashfs-xz"

IMAGE_FEATURES += "ssh-server-openssh"

NO_RECOMMENDATIONS = "1"

EXTRA_IMAGE_FEATURES = ""

inherit core-image

IMAGE_INSTALL = "\
 xm-guest-init \
 xm-guest-ssh \
 xm-guest-boot \
 xm-virtio-sshd \
 python3-modules \
 packagegroup-core-boot \
 systemd-conf \
 kernel-modules \
 openssh-sshd \
 \
 e2fsprogs-mke2fs \
 btrfs-tools \
 squashfs-tools \
 \
 coreutils \
 findutils \
 bash \
 rsync \
"
