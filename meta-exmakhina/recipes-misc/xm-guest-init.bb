# SPDX-License-Identifier: MIT
# Copyright (c) 2020-2021 Jérôme Carretero & contributors

SUMMARY = "Pivot root to R/W with overlayfs"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

S = "${WORKDIR}"

do_install() {
	cd ${WORKDIR}

	cat << EOF > init.sh
#!/bin/sh

if test -n \${init_override}; then
  eval "\$(echo \${init_override} | base64 -d)"
fi

PATH=/bin:/sbin:/usr/bin:/usr/sbin
mount -t proc proc /proc
mount -t sysfs sysfs /sys
#mount -t devtmpfs none /dev
mount -t tmpfs tmpfs_mnt /mnt
mkdir /mnt/ro /mnt/rw /mnt/work /mnt/rootfs
mount --bind / /mnt/ro
mount -t overlay overlay -o lowerdir=/mnt/ro,upperdir=/mnt/rw,workdir=/mnt/work /mnt/rootfs
cd /mnt/rootfs

mkdir ./home/root/mnt
mount -t 9p -o trans=virtio,version=9p2000.L xm-guest ./home/root/mnt

if test -n \${init_after_mount}; then
  ./home/root/mnt/\${init_after_mount}
fi

mkdir -p ./home/root/.ssh
chmod 700 ./home/root/.ssh
echo "ssh-ed25519 \${id_ed25519_pub}" >> ./home/root/.ssh/authorized_keys
chmod 600 ./home/root/.ssh/authorized_keys

mount -n --move /proc ./proc
mount -n --move /sys ./sys
mount -n --move /dev ./dev

cat << HAHA > ./etc/systemd/network/qemu.network
[Match]
Name=eth0

[Network]
DHCP=yes
HAHA

mkdir old_root
pivot_root . old_root
echo Chrooting
exec chroot . sh -c 'umount /old_root; exec /lib/systemd/systemd' <dev/console >dev/console 2>&1
EOF

	install -d "${D}/sbin"
	install -m 0755 ${WORKDIR}/init.sh ${D}/sbin/init
}

FILES_${PN} += "/sbin/init"
