# SPDX-License-Identifier: MIT
# Copyright (c) 2020-2021 Jérôme Carretero & contributors

SUMMARY = "SSH configuration data"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI=" \
 file://ssh_host_dsa_key \
 file://ssh_host_dsa_key.pub \
 file://ssh_host_rsa_key \
 file://ssh_host_rsa_key.pub \
 file://ssh_host_ecdsa_key \
 file://ssh_host_ecdsa_key.pub \
 file://ssh_host_ed25519_key \
 file://ssh_host_ed25519_key.pub \
 file://id_ed25519 \
 file://id_ed25519.pub \
"

S = "${WORKDIR}"

do_install() {
	install -d "${D}/${sysconfdir}/ssh"
	for y in dsa rsa ecdsa ed25519; do
		install -m 0644 ${WORKDIR}/ssh_host_${y}_key.pub "${D}/${sysconfdir}/ssh"
		install -m 0600 ${WORKDIR}/ssh_host_${y}_key "${D}/${sysconfdir}/ssh"
	done

	install -m 0700 -d "${D}/home/root/.ssh"
	cat ${WORKDIR}/id_ed25519.pub >> ${WORKDIR}/authorized_keys
	install -m 0600 ${WORKDIR}/authorized_keys "${D}/home/root/.ssh"
}

FILES_${PN} = " \
 ${sysconfdir} \
 /home \
"
