# SPDX-License-Identifier: MIT
# Copyright (c) 2020-2021 Jérôme Carretero & contributors

SUMMARY = "Boot misc"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

do_install() {
	install -d ${D}${sysconfdir}/systemd/system
	ln -sf /dev/null ${D}${sysconfdir}/systemd/system/serial-getty@ttyS1.service

	install -d ${D}${sysconfdir}/systemd/system/serial-getty@ttyS0.service.d
	cat << EOF > ${D}${sysconfdir}/systemd/system/serial-getty@ttyS0.service.d/autologin.conf
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin root -8 -L %I 115200 \$TERM
TTYVTDisallocate=no
EOF
}

FILES_${PN} = " \
 ${sysconfdir} \
"
