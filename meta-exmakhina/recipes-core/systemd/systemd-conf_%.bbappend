do_install_append() {
	# Log to journal
	printf '[Manager]\nDefaultStandardOutput=journal\nDefaultStandardError=journal\n' > ${WORKDIR}/02-stdio.conf
	install -m0644 ${WORKDIR}/02-stdio.conf ${D}${systemd_unitdir}/system.conf.d

	# Ensure storage doesn't eat up all space
	printf '[Journal]\nSystemMaxUse=2G\nSystemKeepFree=500M\n' > ${WORKDIR}/01-storage.conf
	install -m0644 ${WORKDIR}/01-storage.conf ${D}${systemd_unitdir}/journald.conf.d
}
