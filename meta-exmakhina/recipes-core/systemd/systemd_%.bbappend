# Minimalist setup
PACKAGECONFIG = "kmod networkd resolved"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append = " \
 file://journal.conf \
"

FILES_${PN}_append = "${sysconfdir}/tmpfiles.d/journal.conf"

# To remove warnings about missing group
GROUPADD_PARAM_${PN} += "; -r render"

patch_do_patch_append() {
    import subprocess

    # Remove warning about missing module, which we don't need
    cmd = ["sed", "-i", "-e" "/autofs4/d", "{}/src/core/kmod-setup.c".format(s)]
    subprocess.call(cmd)
}

do_install_append() {
	# We create a tmpfiles.d entry for /var/log/journal,
	# which will be bind-mounted for journald persistence
	install -d ${D}${sysconfdir}/tmpfiles.d
	install -m 0644 ${WORKDIR}/journal.conf ${D}${sysconfdir}/tmpfiles.d

	# Remove warning about BPF firewalling, which we don't have
	sed -i -e '/IPAddressDeny=any/d' ${D}${systemd_unitdir}/system/*.service
}

# cf. xm-guest-init.bb

do_install_append() {
	rm ${D}/sbin/init
}

FILES_${PN}_remove = "/sbin/init"
