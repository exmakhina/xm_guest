#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# VM accessor
# SPDX-License-Identifier: GPL-3.0
# Copyright (c) 2020-2021 Jérôme Carretero & contributors

import sys, io, os, subprocess, argparse, logging, time, shlex

logger = logging.getLogger(None if __name__ == "__main__" else __name__)

datadir = os.path.dirname(os.path.realpath(__file__))


def list2cmdline(lst):
	return " ".join(shlex.quote(x) for x in lst)


def do_build(args):
	os.chdir(datadir)

	cmd = ["sh", "-c", "cd build_template && . ./source_me && bitbake xm-guest-image"]
	subprocess.run(cmd)
	cmd = ["cp", "--reflink=auto", "--force",
	 "build_template/tmp/deploy/images/qemux86-64/xm-guest-image-qemux86-64.squashfs-xz",
	 ".",
	]
	subprocess.run(cmd)
	cmd = ["cp", "--reflink=auto", "--force",
	 "build_template/tmp/deploy/images/qemux86-64/bzImage-qemux86-64.bin",
	 ".",
	]
	subprocess.run(cmd)
	cmd = ["cp", "--reflink=auto", "--force",
	 "meta-exmakhina/recipes-connectivity/files/xm_stdio_multiplex.py",
	 ".",
	]
	subprocess.run(cmd)
	cmd = ["cp", "--reflink=auto", "--force",
	 "meta-exmakhina/recipes-misc/xm-guest-ssh/id_ed25519",
	 ".",
	]
	subprocess.run(cmd)
	cmd = ["cp", "--reflink=auto", "--force",
	 "meta-exmakhina/recipes-misc/xm-guest-ssh/id_ed25519.pub",
	 ".",
	]
	subprocess.run(cmd)


def do_connect(args):
	cmd = [
	 "ssh",
	]

	if args.port is not None:
		cmd += [
		 "-o", f"Port={args.port}",
		]
	else:
		cmd += [
		 "-o", "ProxyCommand=socat stdio unix-connect:tty.socket",
		]

	cmd += [
	 "-o", "HostKeyAlias=xm-guest",
	 "-o", "IdentitiesOnly=yes",
	 "-o", f"IdentityFile={datadir}/id_ed25519",
	 "root@localhost",
	] + args.rem
	subprocess.call(cmd)


def do_run(args):

	with io.open(f"{datadir}/id_ed25519.pub", "r") as fi:
		a, b, c = fi.read().strip().split()
		id_ed25519 = b

	import base64

	init_override = """
echo "init called with env:"
env
""".strip()

	append = list2cmdline([
	 "root=/dev/sda",
	 "net.ifnames=0", "biosdevname=0",
	 "console=ttyS0,115200n8",
	 "printk.time=1",
	 "earlycon=",
	 "earlyprintk=serial,ttyS0,115200",
	 f"id_ed25519_pub={id_ed25519}",
	 "pouet=a b c",
	 "init_override={}".format(base64.b64encode(init_override.encode("utf-8")).decode())
	])

	cmd = [
	 "qemu-system-x86_64",
	 "-kernel", f"{datadir}/bzImage-qemux86-64.bin",
	 "-append", append,
	 "-enable-kvm",
	 "-M", "q35",
	 "-cpu", "host",
	 "-m", "512",
	 #"-smp", "cpus=2",
	]
	cmd_ = []

	cmd += [
	 "-object", "iothread,id=iothread0",
	 "-device", "virtio-scsi-pci,iothread=iothread0,num_queues=8",
	 "-device", "scsi-hd,drive=disk,rotation_rate=1",
	 "-drive", f"id=disk,file={datadir}/xm-guest-image-qemux86-64.squashfs-xz,if=none,format=raw,readonly",
	]

	cmd_ += [
	 "-drive", f"id=disk,file={datadir}/xm-guest-image-qemux86-64.squashfs-xz,if=virtio,format=raw",
	]


	if args.port:
		cmd += [
		 "-device", "virtio-net-pci,netdev=mynet0",
		]

		cmd_ += [
		 "-device", "e1000,netdev=mynet0",
		]
		cmd += [
		 "-netdev", "user,id=mynet0,hostfwd=tcp::2222-:22",
		]
	else:
		cmd += [
		 "-nic", "none",
		]

	cmd += [
	 "-rtc", "base=localtime",
	 "-virtfs", "local,path=.,mount_tag=xm-guest,security_model=none",
	 "-nographic",
	 "-monitor",
	  "unix:xm-guest-monitor.sock,server,nowait",
	 "-serial",
	  "unix:xm-guest-console.sock,server,nowait",
	  #"stdio",
	 "-device", "virtio-serial",
	 "-chardev", "socket,path=xm-guest-port.sock,server,nowait,id=bar",
	 "-device", "virtserialport,name=org.fedoraproject.port.0,id=port0,chardev=bar",
	]

	cmd += args.rem

	logger.info("Running %s", cmd)

	proc_qemu = None
	proc_proxy = None
	try:
		proc_qemu = subprocess.Popen(cmd,
		 stdin=subprocess.PIPE,
		)

		if args.port is None:
			while True:
				if os.path.exists("xm-guest-port.sock"):
					break
				time.sleep(0.1)

			cmd_proxy = [
			 "socat",
			  "unix-connect:xm-guest-port.sock",
			  f"exec:{datadir}/xm_stdio_multiplex.py clientd",
			]
			proc_proxy = subprocess.Popen(cmd_proxy,
			)

		while True:
			if os.path.exists("xm-guest-console.sock"):
				break
			time.sleep(0.1)

		cmd = [
		 "socat",
		  "stdio,raw,echo=0,escape=0x0f",
		  "unix-connect:xm-guest-console.sock",
		]

		subprocess.call(cmd)


	finally:
		if proc_qemu.poll() is None:
			proc_qemu.terminate()
		if proc_proxy is not None and proc_proxy.poll() is None:
			proc_proxy.terminate()


def main():

	parser = argparse.ArgumentParser(
	 description="xm_guest VM accessor",
	)

	parser.add_argument("--log-level",
	 default="WARNING",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)


	subp = subparsers.add_parser(
	 "build",
	 help="Build VM",
	)

	subp.set_defaults(func=do_build)


	subp = subparsers.add_parser(
	 "run",
	 help="Run VM",
	)

	subp.set_defaults(func=do_run)

	subp.add_argument("--port",
	 help="Port to forward SSH on",
	 type=int,
	)

	subp.add_argument("rem",
	 help="Extra arguments",
	 nargs=argparse.REMAINDER,
	)


	subp = subparsers.add_parser(
	 "ssh",
	 help="Connect to VM",
	)


	subp.set_defaults(func=do_connect)

	subp.add_argument("--port",
	 help="Port to connect to",
	 type=int,
	)

	subp.add_argument("rem",
	 help="Extra arguments",
	 nargs=argparse.REMAINDER,
	)


	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.root.setLevel(getattr(logging, args.log_level))

	if 1:
		logging.basicConfig(
		 level=getattr(logging, args.log_level),
		 format="%(levelname)s %(message)s"
		)

	if getattr(args, 'func', None) is None:
		parser.print_help()
		raise SystemExit(1)
	else:
		args.func(args)
		raise SystemExit()


if __name__ == '__main__':
	ret = main()
	raise SystemExit(ret)
