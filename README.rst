########
xm_guest
########


xm_guest is a VM-based "prototype" allowing to play with filesystem images
without requiring root privileges.


It consists in:

- VM launcher that allows to run commands in a VM.

  The VM is accessible either on local TCP ports, or on a UNIX domain
  socket, which allows to not have less worries about port numbers or security.

- Minimalistic Linux rootfs (built with Yocto).

  Static SSH keys are provisioned in the rootfs.



Installation
############


See `Yocto Project Quick Start
<https://www.yoctoproject.org/docs/current/yocto-project-qs/yocto-project-qs.html>`_
for dependencies necessary to build the image.

.. code:: sh

   ./xm_guest.py build

Other dependencies:

- qemu (with virtfs)
- socat


Usage
#####


.. code:: sh

   ./xm_guest.py run


.. code:: sh

   ./xm_guest.py ssh


Design
######

